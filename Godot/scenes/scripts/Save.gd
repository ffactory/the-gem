extends Node2D

const SAVE_PATH = "res://save.json"

func _ready():
	load_data()

# Resets the save; for debugging purpose

func delete_data():
		var dir = Directory.new()
		dir.remove("res://save.json")
		get_node("/root/Main").available_levels = 1

func save_data():
	var save_dict = {}
	var nodes_to_save = get_tree().get_nodes_in_group("save")
	for node in nodes_to_save:
		save_dict[node.get_path()] = node.save()
	var save_file = File.new()
	save_file.open(SAVE_PATH, File.WRITE)
	save_file.store_line(to_json(save_dict))
	save_file.close()
	
func load_data():
	var save_file = File.new()
	if not save_file.file_exists("res://save.json"):
		return
	save_file.open("res://save.json", File.READ)
	var data = {}
	data = parse_json(save_file.get_as_text())
	for node_path in data.keys():
		var node = get_node(node_path)
		for variable in data[node_path]:
			node.set(variable, data[node_path][variable])