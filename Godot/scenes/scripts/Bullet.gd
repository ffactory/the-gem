extends StaticBody2D

signal player_hit

var bullet_speed
var player_node 


func _ready():
	connect("player_hit", get_parent(), "_on_player_hit")
	bullet_speed = get_parent().bullet_speed
	#$Timer.start()
	player_node = get_node("/root/Main/Level " + String(get_node("/root/Main").current_level) + "/Player")
	var time = get_parent().timer_time
	if not time == 0:
		$Timer.set_wait_time(time) 
		$Timer.start()
		
# Bullet movement
func _process(delta):
	position.x = get_position().x + bullet_speed
	var pos_bullet = get_position() + get_parent().get_position()
	var pos_player = player_node.get_position()
	var distanceToPlayer = pos_bullet.distance_to(pos_player)
	if distanceToPlayer > 5000:
		queue_free()

# Detects collision with the player
func _on_Hit_Area_body_entered(body):
	if body.get_name() == "Player":
		emit_signal("player_hit")
		
# Deletes bullet when the timer runs out
func _on_Timer_timeout():
	queue_free()
