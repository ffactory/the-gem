extends Area2D

signal goal_reached

func _ready():
	connect("body_entered", get_node("/root/Main"), "_on_goal_reached")	
	$AnimatedSprite.play()