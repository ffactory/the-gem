extends KinematicBody2D

export(int) var min_x # heighest point
export(int) var max_x # lowest point
export(int) var speed = 2

var left = true
var on = true

func _physics_process(delta):
	if left == true and on == true:
		position.x -= speed
		if position.x <= min_x:
			on = false
			left = false
			$PauseTime.start()
	if left == false and on == true:
		position.x += speed
		if position.x >= max_x:
			on = false
			left = true
			$PauseTime.start()
	
func _on_PauseTime_timeout():
	on = true
