extends KinematicBody2D

export(int) var min_y # heighest point
export(int) var max_y # lowest point
export(int) var speed = 2

var up = true
var on = true

onready var last_y = get_global_position().y

func _physics_process(delta):
	if up == true and on == true:
		position.y -= speed
		if position.y <= min_y:
			on = false
			up = false
			$PauseTimer.start()
	if up == false and on == true:
		position.y += speed
		if position.y >= max_y:
			on = false
			up = true
			$PauseTimer.start()
			
	if last_y > position.y:
		$Sprite.position.y = speed
	elif last_y < position.y:
		$Sprite.position.y = -speed
	else:
		$Sprite.position.y = 0
		
	last_y = position.y
	
func _on_PauseTime_timeout():
	on = true