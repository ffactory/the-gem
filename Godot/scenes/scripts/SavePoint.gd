extends Area2D

var hoisted = false

signal spawnpoint

func _ready():
	connect("spawnpoint", get_parent().get_parent().get_node("Player"), "_change_spawnpoint")

func _on_SavePoint_body_entered(body):
	if body.get_name() == "Player" and not hoisted:
		$Sprite/FlagAni.play("HoistFlag")
		hoisted = true
		emit_signal("spawnpoint")


func _on_FlagAni_animation_finished(anim_name):
	$Sprite/FlagAni.play("WaveFlag")
