extends TileMap

var array = [[0, 0], [32, 0], [64, 0], [0, 32], [32, 32], [64, 32], [0, 64], [32, 64], [64, 64], [0, 96], [32, 96]]

var a = 0

func _on_Timer_timeout():
	tile_set.tile_set_region(4, Rect2(Vector2(array[a][0], array[a][1]), Vector2(32, 32)))
	a += 1
	if a > 10:
		a = 0
