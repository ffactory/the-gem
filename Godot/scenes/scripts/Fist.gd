extends KinematicBody2D

const SPEEDDOWN = 10
const SPEEDUP = 2

var smash = false
var returning = false
var wait = false
var start_position_y
var last_y 

export(int) var ray_length = 150

signal player_hit

func _ready():
	$Right.cast_to.y = ray_length + 30
	$Middle.cast_to.y = ray_length 
	$Left.cast_to.y = ray_length + 30
	var levelname = get_parent().get_name()
	connect("player_hit", get_node("/root/Main/"+ levelname +"/Player"), "_on_player_hit")
	start_position_y = get_position().y
	last_y = start_position_y

func _physics_process(delta):
	
	if $Right.is_colliding():
		var col_right = $Right.get_collider()
		test_for_player(col_right)
	if $Middle.is_colliding():
		var col_middle = $Middle.get_collider()
		test_for_player(col_middle)
	if $Left.is_colliding():
		var col_left = $Left.get_collider()
		test_for_player(col_left)
		
	if not wait:
		if smash:
			var collision_movement = false
			if $Floor.is_colliding():
				var floor_col = $Floor.get_collider()
				if floor_col.is_in_group("Floor"):
					collision_movement = true
			if not collision_movement:
				position.y += SPEEDDOWN
			else:
				move_and_collide(Vector2(0, 10))
		elif returning:
			position.y -= SPEEDUP
			if position.y < start_position_y:
				returning = false
				enable_RayCasts()

	if last_y > position.y:
		$Sprite.position.y = 1
	else:
		$Sprite.position.y = 0
	last_y = position.y

func test_for_player(col):
	if col.get_name() == "Player":
		smash = true
		disable_RayCasts()
		$HitArea/Hitbox.disabled = false
		
func _on_HitArea_body_entered(body):
	if body.get_name() == "Player":
		emit_signal("player_hit")
	else:
		smash = false
		returning = true
		$HitArea/Hitbox.disabled = true
		wait = true
		$Timer.start()
		
func _on_Timer_timeout():
	wait = false
	
func enable_RayCasts():
	$Right.enabled = true
	$Middle.enabled = true
	$Left.enabled = true

func disable_RayCasts():
	$Right.enabled = false
	$Middle.enabled = false
	$Left.enabled = false

