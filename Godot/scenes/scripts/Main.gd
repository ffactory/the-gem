extends Node2D

const bUseGeneratedLevels = false

var levels = []
var current_level = 1
var available_levels = 1
var level_node
var game_state = "running"
var menu_opened = true
var level_exists = false
var savepoints_active = true
#var videoplayerStart = false
#var videoplayerActive = false

#onready var videoplayer = $"Menu/Startscreen player"
onready var camera = $Menu/Camera2D
onready var levelSelection = $Menu/LevelselectionContainer
onready var mainMenu = $Menu/MainMenuContainer
onready var play_button = $Menu/MainMenuContainer/ButtonOrder/PlayContinue/Play
onready var continue_button = $Menu/MainMenuContainer/ButtonOrder/PlayContinue/Continue
onready var delete_button = $Menu/MainMenuContainer/ButtonOrder/DeleteSave/Delete
onready var unlock_box = $Menu/MainMenuContainer/ButtonOrder/DeleteSave/Unlock
onready var hard_button = $Menu/MainMenuContainer/ButtonOrder/EasyHard/Hard
onready var easy_button = $Menu/MainMenuContainer/ButtonOrder/EasyHard/Easy
onready var levelselectionRows = $Menu/LevelselectionContainer/LevelselecitonRowContainer

func _ready():
	start_up()
	
func _process(delta):
	if Input.is_action_just_pressed("pause") and menu_opened == false:
		pause()
	if Input.is_action_just_pressed("ui_cancel") and level_exists:
		 toggle_menu()
	
#	print('vorher' + str(videoplayerStart))
#	if not videoplayer.is_playing() and videoplayerActive:
#		splashPlay()
#	elif not videoplayer.is_playing() and videoplayerActive:
#		splashPlay()

#	elif videoplayerStart:
#		splashStop()
		
#	print('nachher' + str(videoplayerStart))

func start_up():
#	videoplayerStart = true
#	videoplayerActive = true
	if bUseGeneratedLevels == true:
		levels.append(load("res://scenes/levels/generated level.tscn"))
	else:
		# Loop through elemets in /levels and count the levels themselves
		var i = 0
		levels.resize(getAllElements("res://scenes/levels/").size())
		for element in getAllElements("res://scenes/levels/"):
			if element.find('Level') != -1:
				levels.insert(i, element)
				i += 1
	camera.current = true

# Handle changing Levels
func changeLevel(number):
#	splashStop()
	if levels[number] != null:
		removeLevels()
		current_level = number
		level_node = "Level " + String(current_level) 
		addLevel(number)
		
		var savepoints = get_node("/root/Main/" + str(level_node)+ '/Savepoints').get_children()
		if not savepoints_active:
			for savepoint in savepoints:
				savepoint.visible = false
				savepoint.get_node("Hitbox").disabled = true
		else:
			for savepoint in savepoints:
				savepoint.visible = true
				savepoint.get_node("Hitbox").disabled = false
	else:
		print("Dieses Level existiert nicht")

func removeLevels():
	if level_exists:
		remove_child(get_node(level_node))
	level_exists = false
	
func addLevel(number):
	var test = levels[number]
	var newLevel = load(test).instance()
	add_child(newLevel)
	level_exists = true
# Done handling levels


# Waits for timer
func _on_done_loading():
	startUp()

# Changes the level when the goal is reached
func _on_goal_reached(body):
	call_deferred("next_level", body)
		
func next_level(body):
	if body.get_name() == "Player" and current_level < levels.size() - 1: #later "levels.size()" cause we start at level 1
		changeLevel(current_level + 1)
		if current_level > available_levels:
			available_levels += 1
		get_child(0).save_data()
	else:
		print("no level available yet")

# Pauses the game on pressing "P"
func pause():
	var delay = false # Prevents the game from directly unpausing itself again
	if game_state == "running":
		get_tree().paused = true
		game_state = "paused" # Geht natürlich auch mit einem Boolean, aber das fand ich "ästehtischer"
		delay = true
	if game_state == "paused" and delay == false:
		get_tree().paused = false
		game_state = "running"
		
func save():
	var save_dict = {
		"available_levels" : available_levels
	}
	return save_dict


# Handles the Menu ----------------------------

# Starts up the game
func _on_Play_pressed():
#	print('videoplayerActive: ' + str(videoplayerActive))
	changeLevel(available_levels)
	$Menu.hide()
	menu_opened = false

# Opens/closes the menu on pressing "escape"
func toggle_menu():
	if menu_opened == false:
		if level_exists:
			get_node(level_node).hide()
#		splashPlay()
		camera.current = true
		$Menu.show()
		levelSelection.hide()
		mainMenu.show()
		play_button.hide()
		continue_button.show()
		get_tree().paused = true
		menu_opened = true
	else:
		continue_game()
	
func _on_Continue_pressed():
	continue_game()

# Closes the menu and continues the game
func continue_game():
#	splashStop()
	$Menu.hide()
	get_node(level_node+"/Player/Camera2D").current = true
	get_node(level_node).show()
	get_tree().paused = false
	game_state = "running"
	menu_opened = false

func _on_SelectLevel_pressed():
	mainMenu.hide()
	levelSelection.show()
	for child in levelselectionRows.get_children():
		for button in child.get_children():
			if int(button.get_name()) > available_levels:
				button.set_disabled(true)
			else:
				button.set_disabled(false)

func _on_Return_pressed():
	levelSelection.hide()
	mainMenu.show()

func _on_select_button_pressed(number):
	select_level(number)

# Controls the level select
func select_level(level):
	current_level = level
	changeLevel(current_level)
	get_tree().paused = false
	$Menu.hide()
	if level_exists:
		get_node(level_node + "/Player/Camera2D").current = true
	mainMenu.show()
	levelSelection.hide()
	menu_opened = false
	
#func splashPlay():
#	videoplayer.play()
#	videoplayerActive = true

#func splashStop():
#	videoplayer.stop()
#	videoplayerActive = false

	
func _on_Delete_pressed():
	get_child(0).delete_data()
	delete_button.disabled = true
	unlock_box.pressed = false
	current_level = 1
	changeLevel(current_level)
	get_node(level_node).hide()
	camera.current = true
	
func _on_CheckBox_toggled(button_pressed):
	if button_pressed == true:
		delete_button.disabled = false
	else:
		delete_button.disabled = true

func _on_Easy_pressed():
	easy_button.visible = false
	easy_button.disabled = true
	hard_button.visible = true
	hard_button.disabled = false
	savepoints_active = false
	
func _on_Hard_pressed():
	hard_button.visible = false
	hard_button.disabled = true
	easy_button.visible = true
	easy_button.disabled = false
	savepoints_active = true

#Done handeling the Menu ------------------------------------

# Tools
func getAllElements(path):
	var elements = []
	var elementDir = Directory.new()
	elementDir.open(path)
	elementDir.list_dir_begin()

	var allElementsSearched = false
	while not allElementsSearched:
		var element = elementDir.get_next()
		if element == "":
			allElementsSearched = true
			break
		elif not element.begins_with('.'):
			elements.append(path + element)
	
	elementDir.list_dir_end()
	return elements


# Testing/Debugging functions ---------------------------------- should be at the end of code








