extends StaticBody2D

signal player_hit

const BULLET_SPEED = 3

var bullet_ = preload("res://scenes/Bullet.tscn")
var player_node
var bullet_speed = BULLET_SPEED

export(float) var time = 3
export(float) var timer_time = 0

func _ready():
	player_node = get_node("/root/Main/Level " + String(get_node("/root/Main").current_level) + "/Player")
	connect("player_hit", player_node, "_on_player_hit")
	$Timer.wait_time = time
	
# Creates the bullet
func _on_Timer_timeout():
	var bullet = bullet_.instance()
	add_child(bullet)

# Passes on the signal of the bullet
func _on_player_hit():
	emit_signal("player_hit")
	
# Turns left or right depending on the player´s position
func _process(delta):
	if position.x - player_node.get_position().x > 0:
		$Default.flip_h = true
		bullet_speed = -BULLET_SPEED
	else:
		$Default.flip_h = false
		bullet_speed = BULLET_SPEED