extends Node2D

signal done_loading

func _ready():
	connect("done_loading", get_node("/root/Main"), "_on_done_loading")

func _on_done_loading():
	emit_signal("done_loading")
