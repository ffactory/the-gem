extends KinematicBody2D

const GRAV = 1400
const JUMP = -500
const MOVE = 150

export(int) var min_x 
export(int) var max_x 
export(bool) var left = true

var velocity = Vector2()
var jump_now = false

signal player_hit

func _ready():
	null_pointer_handling()
	var levelname = get_parent().get_name()
	connect("player_hit", get_node("/root/Main/"+levelname+"/Player"), "_on_player_hit")
	
func _physics_process(delta):
	
	var force = Vector2(0, GRAV)
	
	if jump_now:
		velocity.y = JUMP
		jump_now = false
		$JumpTimer.start()
	
	if not is_on_floor():
		if left == true:
			force.x = - MOVE
		else:
			force.x = MOVE
	else:
		if position.x >= max_x:
			left = true
		if position.x <= min_x:
			left = false
	
	velocity.y += force.y * delta
	velocity.x = force.x
	velocity = move_and_slide(velocity, Vector2(0, -1), 5, 4, 0.87266462599716)

	
func _on_Timer_timeout():
	jump_now = true

func null_pointer_handling():
	if min_x == null or max_x == null:
		print(get_name()+ ": min_x or max_x is null")
		min_x = 0
		max_x = 0
		queue_free()
	
func _on_Hit_Area_body_entered(body):
	if body.get_name() == "Player":
		emit_signal("player_hit")
