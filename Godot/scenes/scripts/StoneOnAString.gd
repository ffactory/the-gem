extends StaticBody2D

export(float) var speed = 6
export(bool) var clockwise = true

signal player_hit

func _ready():
	var levelname = get_parent().get_name()
	connect("player_hit", get_node("/root/Main/"+levelname+"/Player"), "_on_player_hit")
	

func _physics_process(delta):
	if clockwise:
		rotation_degrees += speed
	else:
		rotation_degrees += -speed
		
func _on_Area2D_body_entered(body):
	if body.get_name() == "Player":
		emit_signal("player_hit")
