extends TileMap


var top_left_coords = [[0, 0], [32, 0], [0, 32], [32, 32]]

var a = 0

func _on_Timer_timeout():
	tile_set.tile_set_region(25, Rect2(Vector2(top_left_coords[a][0], top_left_coords[a][1]), Vector2(32, 32)))
	a += 1
	if a > 3:
		a = 0
