extends KinematicBody2D

const GRAV = 1200
const WALK_SPEED = 225

var velocity = Vector2()

export(bool) var left = true
export(int) var min_x 
export(int) var max_x 

signal player_hit

func _ready():
	var levelname = get_parent().get_name()
	connect("player_hit", get_node("/root/Main/"+levelname+"/Player"), "_on_player_hit")
	null_pointer_handling()

# Gravity
func _physics_process(delta): 

	var force = Vector2(0, GRAV)

	velocity.y += force.y * delta
	velocity.y = clamp(velocity.y, 0, GRAV)

	if is_on_floor():
		velocity.y = 0
		if left == true:
			force.x = - WALK_SPEED
			if position.x < min_x:
				left = false
		else:
			force.x = WALK_SPEED
			if position.x > max_x:
				left = true

	velocity.x = force.x 

	velocity = move_and_slide(velocity, Vector2(0, -1))

# Detects collision with the player
func _on_Hit_Area_body_entered(body):
	if body.get_name() == "Player":
		emit_signal("player_hit")

func null_pointer_handling():
	if min_x == null or max_x == null:
		print(get_name()+ ": min_x or max_x is null")
		queue_free()