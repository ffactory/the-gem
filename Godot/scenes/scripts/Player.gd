extends KinematicBody2D

const GRAV = 1400
const JUMP = 122
const WALK_SPEED = 250
const MAX_AIRBORNE_TIME = 0.155
const SPAWNPOS = Vector2()
const SCREENSIZE = Vector2()

var time_in_air = 0
var jump_decrease = JUMP / 10
var jump_decrease_increase = JUMP/20 + jump_decrease / 2
var in_air = false
var walk_speed = WALK_SPEED
var jump = JUMP
var velocity = Vector2()
var sprites = {}
var lastState = ['right','default']
var currentState = ['right','default']
var wall_jumped = false
var wall_bounce = false
var wall_jump_boost = 100
var wall_jump_time = 0
var on_up_down_platform = false 
var on_slope = false
var oneWayTiles
var wall_left = false
var wall_right = false
var crouching = false
var deathCount = 0
var reset_delay = true
var wall_jump_delay = 0

func _ready():
	set_start_variables()
	find_OneWayTiles()
	
func _physics_process(delta): 
	
	#Reset player on 'R'
	if Input.is_action_just_pressed("reset_player"):
		resetPlayer()
		return
	
	var up = Input.is_action_pressed("ui_up")
	var left = Input.is_action_pressed("ui_left")
	var right = Input.is_action_pressed("ui_right")
	var down = Input.is_action_pressed("ui_down")	
	var nothing_above = not $LeftUpRayCast.is_colliding() and not $RightUpRayCast.is_colliding()
	var on_floor = $LeftDownRayCast.is_colliding() or $RightDownRayCast.is_colliding()
	var crouch_nothing_above = not $CrouchLeftUpRayCast.is_colliding() and not $CrouchRightUpRayCast.is_colliding()
	
	# Handels upwards and downwards moving platforms
	if get_floor_velocity().y < 0:
		on_up_down_platform = true
	else:
		on_up_down_platform = false
	
	# Prevents the player from getting stuck beneath a downward moving platform
	if in_air: 
		if $RightUpRayCast.is_colliding():
			var col = $RightUpRayCast.get_collider()
			if col.get_children().size() == 4:
				if col.get_child(3).get_name() == "CheckNode":
					position.y += 2.7
		if $LeftUpRayCast.is_colliding():
			var col = $LeftUpRayCast.get_collider()
			if col.get_children().size() == 4:
				if col.get_child(3).get_name() == "CheckNode":
					position.y += 2.7
				
	var force = Vector2(0, GRAV)
	var currentPosition = get_global_transform()[2]

	if is_on_wall() and Input.is_action_just_pressed("ui_up") and not is_on_floor() and wall_jumped == false and $WallJumpDelay.get_time_left() == 0:
		wall_jumped = true
		wall_bounce = true
		velocity.y = -JUMP * 3.75
		velocity.y = clamp(velocity.y, -JUMP * 3.75, velocity.y) # Verhindert das "hochboosten" an Wänden
		$WallJumpDelay.start()
	elif not is_on_wall():
		wall_jumped = false

	if wall_bounce == false:
		if right:
			force.x = move_right(force, 0)
			
		elif left:
			force.x = move_left(force, 0)
	else:
		if currentState[0] == 'right':
			force.x = move_left(force, wall_jump_boost)
			currentState[0] = 'right'
		if currentState[0] == 'left':
			force.x = move_right(force, wall_jump_boost)
			currentState[0] = 'left'
		if wall_jump_time > 0.25:
			wall_bounce = false
			wall_jump_time = 0
		wall_jump_time += delta

	# x- and y- Movement
	velocity.y += force.y * delta
	velocity.x = force.x 
	velocity = move_and_slide(velocity, Vector2(0, -1), 5, 4, 0.87266462599716)


	if is_on_floor():
		if up:
			currentState[1] = 'jumping'
		else:
			currentState[1] = 'default'
		in_air = false
		time_in_air = 0
		jump_decrease = 0
		jump = JUMP

		# Crouch action
		if down: # crouch sprite 
			walk_speed = crouchState()
		elif not down:
			normalState()
			walk_speed = WALK_SPEED

		if down:
			crouch_hitbox()
		else:
			if nothing_above and not down:
				normal_hitbox()
			elif not get_floor_velocity().y < 0:
				walk_speed = crouchState()
		wall_jumped = false
	else:
		change_one_way_tile(true)
		if not down:
			normalState()


	# Detects if the player is on a slope or not
	var left_vector = $LeftDownRayCast.get_collision_normal()
	var right_vector = $RightDownRayCast.get_collision_normal()
	#print(String(left_vector)+"  "+String(right_vector))
	if (right_vector.y > -1 or left_vector.y > -1) and (left or right) and on_floor:
		on_slope = true
		
	
	# Jumping
	if currentState[1] == 'jumping' and up and time_in_air < MAX_AIRBORNE_TIME and nothing_above:
		if on_up_down_platform:
			position.y -= 5
		walk_speed = WALK_SPEED
		normal_hitbox()
		normalState()
		var slope_jump_boost = 0
		jump_decrease += jump_decrease_increase
		if in_air:
			jump += JUMP - jump_decrease 
		if on_slope:
			slope_jump_boost = 220
		velocity.y = -jump - slope_jump_boost 
		in_air = true
		
	on_slope = false
	time_in_air += delta
	
	# Handels Players death ----------------------------------
	
	# Reset player position after falling out of the floor
	if currentPosition.y > (SCREENSIZE.y + 1000):
		resetPlayer()
	#Resets Player if he is crushed by a moving Platform
	if wall_left and wall_right:
		resetPlayer()
	if not crouching:
		if get_floor_velocity().y < 0 and not nothing_above:
			resetPlayer()
	else:
		if get_floor_velocity().y < 0 and not crouch_nothing_above:
			resetPlayer()
	#------------------------------------------------------------
	
	
# Handles walking and fliping Sprite 
func move_right(force, increase):
	currentState[0] = 'right'
	force.x = walk_speed + increase - get_floor_velocity().x
	for sprite in sprites:
		get_node(sprite).set_flip_h(false)
	return force.x
	
func move_left(force, increase):
	currentState[0] = 'left'
	force.x = -walk_speed - increase - get_floor_velocity().x
	for sprite in sprites:
		get_node(sprite).set_flip_h(true)
	return force.x
# Done handeling walking and fliping Sprite 

# Hitbox handeling -------------
func normal_hitbox():
	$HitBox.disabled = false
	$CrouchHitBox.disabled = true
	$RightArea/Normal.disabled = false
	$RightArea/Crouch.disabled = true
	$LeftArea/Normal.disabled = false
	$LeftArea/Crouch.disabled = true
	
func crouch_hitbox():
	$CrouchHitBox.disabled = false
	$HitBox.disabled = true
	$RightArea/Normal.disabled = true
	$RightArea/Crouch.disabled = false
	$LeftArea/Normal.disabled = true
	$LeftArea/Crouch.disabled = false
#-------------------------------

# Resets player´s movement and position on death
func resetPlayer():
	if reset_delay:
		deathCount += 1
		if deathCount == 1:
			$Camera2D/CenterContainer/DeathCountLabel.visible = true
		var text = $Camera2D/CenterContainer/DeathCountLabel
		text.text = str(deathCount)
		set_global_transform(SPAWNPOS)
		velocity = Vector2(0.0,0.0) 
		reset_delay = false
		$ResetDelay.start()
	
# Sprite and State handling start -------------------------

func hideAllSprites():
	for sprite in sprites:
		get_node(sprite).hide()

func normalState():
	hideAllSprites()
	if velocity.x == 0 or not is_on_floor():
		sprites['Idle'].show()
	else:
		sprites["Walk"].show()
		if not $Walk/WalkAni.current_animation == "WalkAni":
			$Walk/WalkAni.play("WalkAni")
	crouching = false
	  
func crouchState():
	hideAllSprites()
	if velocity.x == 0:
		sprites['Crouch'].show()
	else:
		sprites['CrouchWalk'].show()
		if not $CrouchWalk/CrouchAni.get_current_animation() == "CrouchAni":
			$CrouchWalk/CrouchAni.play("CrouchAni")
	change_one_way_tile(false)
	crouching = true
	return WALK_SPEED/2
	
# Sprite and State handling end ---------------------------
	
# Resets player position on death caused by an enemy
func _on_player_hit():
	resetPlayer()
	
# Sets the start up variables
func set_start_variables():
	sprites = {
		'Idle': $Idle,
		'Walk': $Walk,
		'Crouch': $Crouch,
		'CrouchWalk': $CrouchWalk
	}
	SPAWNPOS = get_global_transform()
	SCREENSIZE = OS.get_window_size()

func change_one_way_tile(boolean): 
	if not oneWayTiles == null:
		var tileset = oneWayTiles.tile_set
		tileset.tile_set_shape_one_way(0, 0 , boolean)

	
func find_OneWayTiles():
	for child in get_parent().get_children():
		if child.get_name() == "OneWayTiles":
			oneWayTiles = child

func _on_SpikeArea_body_entered(body):
	if body.is_in_group("Deadly"):
		resetPlayer()

func _on_RightArea_body_entered(body):
	wall_right = true
	
func _on_RightArea_body_exited(body):
	wall_right = false
	
func _on_LeftArea_body_entered(body):
	wall_left = true

func _on_LeftArea_body_exited(body):
	wall_left = false
	
func _change_spawnpoint():
	SPAWNPOS = get_global_transform()


func _on_ResetDelay_timeout():
	reset_delay = true


