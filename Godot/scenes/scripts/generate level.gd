extends Node2D

const templateBlockSize = 32*9 # px (tilesize * tileblockcount)
const templatePath = "res://scenes/templates/"

var levelSize = Vector2(6,6)
var templates = []

func _ready():
	deleteContents()
	generateLevel()
	#templates = getTemplates()
#	addTemplateBlock(0)
	#add_child(load('res://scenes/levels/templates/1 tl/1.tscn').instance())

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func generateLevel():
	templates = getTemplates()
	
	var currentRow = 0
	addTemplateRow(0, currentRow)
	currentRow += 1
	while currentRow <= (levelSize.y - 2):
		addTemplateRow(3, currentRow)
		currentRow += 1
	addTemplateRow(6, currentRow)
	currentRow += 1 #useless


# Helpers
func addTemplateRow(number, yPosNum):
	var xPosNum = 0
	var xPos = 0
	var yPos = 0
	
	var numberHelper = 1
	
	#top row
	xPos = xPosNum*templateBlockSize
	yPos = yPosNum*templateBlockSize
	addTemplateBlock(number,Vector2(xPos,yPos))
	
	#rows in between
	number += 1
	xPosNum += 1
	while true:
		xPos = xPosNum*templateBlockSize
		yPos = yPosNum*templateBlockSize
		addTemplateBlock(number,Vector2(xPos,yPos))
		xPosNum += 1
		numberHelper += 1
		if not numberHelper <= (levelSize.x - 2):
			break
	
	# bottom row
	number += 1
	xPos = xPosNum*templateBlockSize
	yPos = yPosNum*templateBlockSize
	addTemplateBlock(number,Vector2(xPos,yPos))
	
func addTemplateBlock(number, pos=null):
	var currentBlock = getAndLoadTemplateBlock(number)
	add_child(currentBlock)
	print('Placing: ' + String(number))
	
	if pos != null: # move block if position in given
		currentBlock.set_position(pos)
#		print('moved block to: ' + String(xPos) + '−' + String(yPos))
	return currentBlock

func getAndLoadTemplateBlock(number):
	var randomBlockNumber = randi()%templates[number].size()
	var currentBlockPath = templates[number][randomBlockNumber]
	return load(currentBlockPath).instance()
	#res://scenes/levels/templates/1 tl/1.tscn

func deleteContents():
	pass
	
func getTemplates():
	var _templates = []
	var folders = getAllElements(templatePath)
	
	var i = 0
	for folder in folders:
		_templates.append(getAllElements(folder + '/'))
		i += 1
	
	print(_templates)
	return _templates
	
	
func getAllElements(path):
	var elements = []
	var elementDir = Directory.new()
	elementDir.open(path)
	elementDir.list_dir_begin()

	var allElementsSearched = false
	while not allElementsSearched:
		var element = elementDir.get_next()
		if element == "":
			allElementsSearched = true
			break
		elif not element.begins_with('.'):
			elements.append(path + element)
	
	elementDir.list_dir_end()
	return elements